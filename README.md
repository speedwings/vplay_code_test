#Test Video Player
##INSTALL
Run ```npm install``` to install packeges

##DEVELOP
Run ```npm run dev``` to start webpack dev server on port 8811 with HMR active.

##DEPLOY
Run ```npm run build``` to compile the project in *dist* folder.
