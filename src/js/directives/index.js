export default {
    playing (el, binding) {
        (binding.value) ? el.play() : el.pause()
    },
    muted (el, binding) {
        el.muted = binding.value
    },
    volume (el, binding) {
        el.volume = binding.value
    }
}
