export function Timer (seconds) {
    const h = Math.floor(seconds / (60*60)).toString()
    const m = Math.floor((seconds % (60*60)) / 60).toString()
    const s = Math.ceil(seconds % 60).toString()
    return ( (h !== '0') ? ('00'+h).slice(-2) + ':' : '' )
            + ('00'+m).slice(-2)
            + ':'
            + ('00'+s).slice(-2)
}

export default {
    Timer
}
